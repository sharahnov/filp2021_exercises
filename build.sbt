name := "exercises"

version := "0.1"

scalaVersion in ThisBuild := "2.13.4"

val libraries = Seq("org.scalatest" %% "scalatest-wordspec" % "3.2.3" % Test)

lazy val exercises01 = project in file("exercises01") settings (libraryDependencies ++= libraries)
