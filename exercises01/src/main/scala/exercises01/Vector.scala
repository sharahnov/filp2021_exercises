package exercises01

class Vector(val x: Double, val y: Double) {
  def +(other: Vector): Vector = new Vector(x + other.x, y + other.y)

  def -(other: Vector): Vector = new Vector(x - other.x, y - other.y)

  def *(scalar: Double): Vector = new Vector(x * scalar, y * scalar)

  def unary_- : Vector = new Vector(-x, -y)

  def euclideanLength: Double = Math.sqrt(x * x + y * y)

  def normalized: Vector = new Vector(x /  euclideanLength, y / euclideanLength)

  override def equals(other: Any): Boolean = {
    if (!other.isInstanceOf[Vector]) false else {
      val otherVector = other.asInstanceOf[Vector]
      x == otherVector.x && y == otherVector.y
    }
  }

  override def toString: String = s"Vector($x, $y)"
}

object Vector {
  def fromAngle(angle: Double, length: Double): Vector = new Vector(length * Math.cos(angle), length * Math.sin(angle))

  def sum(list: List[Vector]): Vector = {
    var vectorSum: Vector = new Vector(0, 0)
    for (vector <- list) vectorSum = vectorSum + vector
    vectorSum
  }

  def unapply(arg: Vector): Option[(Double, Double)] = Option(arg.x, arg.y)
}